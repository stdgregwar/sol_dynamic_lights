# Dynamic Lights

A light manager system for solarus that support light casting and normal mapping.

## Recommended use

This quest is built on top of solarus initial_quest. You can either start from scratch
with this project or import the relevant component into your already existing quest.

### Files to import

* data/entities/light.lua
* data/scripts/lights/light_manager.lua
* data/shaders/cast_shadow1d.{dat, frag.glsl, vert.glsl}
* data/shaders/make_shadow1d.{dat, frag.glsl, vert.glsl}
* data/shaders/normal_map.{dat, frag.glsl}

## Usage

Example can be found in the map `first_map`.

Note that sprite on which you enable Normal Mapping must have "double" sprite sheet.

Like this one :
![eldran](data/sprites/hero/eldran.png "Eldran")

Sprites animation must be setted up in to top of the image, the bottom part will be automatically
used if you enable normal mapping for the entity trough the `light_manager`.

## Demo

Just launch the quest with Solarus 1.6.x

