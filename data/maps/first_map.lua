-- example map script demonstrating the use of the light_manager
-- @author stdgregwar


-- get map and hero as always
local map = ...
local hero = map:get_hero()

-- require the light manager script, this could fail if your project does not have all the required shaders
local light_mgr = require"scripts/lights/light_manager"

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  -- init light manager with the map and an ambient light color
  light_mgr:init(map, {20,15,15})
  
  -- add the hero to the list of dynamic entities casting shadows
  light_mgr:add_occluder(hero)
  
  -- add the hero to the list of entities having normal map compatible sprites
  light_mgr:add_normal_mapped(hero)
  
  -- add the test ball to the list as well
  light_mgr:add_normal_mapped(ball)
  
  -- set the ball to be "vertical"
  ball:set_drawn_in_y_order(true)
  
 
  -- iterate trough each switches to give them a light and some color
  -- changing behaviour
  for sw in map:get_entities_by_type'switch' do
    local x,y,l = sw:get_position()
    local acol = {0,255,0}
    local icol = {math.random(20,255), math.random(20,255), math.random(20,255)}
    
    -- this creates a light custom entity with switches position and settings
    local light = map:create_custom_entity{
      layer = l,
      x = x+8,
      y = y+8,
      properties = {
        {
          key = "color",
          value = string.format("%d,%d,%d",unpack(icol)),
        },
        {
          key = "radius",
          value = "80",
        },
      },
      width = 16,
      height = 16,
      direction = 0,
      model = "light",
    }
  
    -- add some action to the switch
    function sw:on_activated()
      light:set_color(acol)
      sol.timer.start(sw, 2000, function()
          sw:set_activated(false)
          sw:on_inactivated()
        end)
    end
    function sw:on_inactivated()
      light:set_color(icol)
    end
  end
end

-- this event called at map drawing end will help the
-- light manager achieve its effect
-- it is mandatory to call light_mgr:draw so that it
-- renders all the light and occluders
function map:on_draw(dst)
  light_mgr:draw(dst, map)
end